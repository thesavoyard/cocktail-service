package com.example.cocktails.model.remote.objects

data class ByCategory(
    val drinks: List<Drink>
) {


    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}
