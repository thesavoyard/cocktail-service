package com.example.cocktails.model.remote.objects

import com.google.gson.annotations.SerializedName

data class Categories(
    @SerializedName("drinks")
    val categories: List<Category>
) {
    data class Category(
        val strCategory: String
    )
}

