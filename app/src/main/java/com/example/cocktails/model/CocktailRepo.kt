package com.example.cocktails.model

import com.example.cocktails.model.remote.CocktailService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object CocktailRepo {
    private val cocktailService = CocktailService.getInstance()

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        return@withContext cocktailService.getCategories()
    }

    suspend fun getDrinksInCategory(category: String) = withContext((Dispatchers.IO)) {
        return@withContext cocktailService.getDrinksInCategory(category)
    }

    suspend fun getOneDrinkById(ID: String) = withContext(Dispatchers.IO) {
        return@withContext cocktailService.getOneDrinkById(ID)
    }

}