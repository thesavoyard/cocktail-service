package com.example.cocktails.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.model.remote.objects.ById
import com.example.cocktails.view.DrinkState
import kotlinx.coroutines.launch

class CocktailViewModel(val repo: CocktailRepo) : ViewModel() {

    private val _drinksState: MutableLiveData<DrinkState> = MutableLiveData(DrinkState())
    val drinksState: LiveData<DrinkState> get() = _drinksState

    fun getCategories() {
        viewModelScope.launch {
            _drinksState.value = DrinkState(
                categories = CocktailRepo.getCategories().categories
            )
        }
    }

    fun getDrinksInCategory(category: String) {
        viewModelScope.launch {
            _drinksState.value = DrinkState(
                drinksInCategory = CocktailRepo.getDrinksInCategory(category).drinks
            )
        }
    }

    fun getOneDrinkById(ID: String) {
        viewModelScope.launch {
            val drink = CocktailRepo.getOneDrinkById(ID).drinks.firstOrNull() ?: ById.Drink()
//            drink.toIngredientsList()
            _drinksState.value = DrinkState(
                oneDrink = drink
            )
        }
    }
}