package com.example.cocktails.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktails.databinding.CategoryItemBinding
import com.example.cocktails.model.remote.objects.Categories

class CategoriesAdapter(
    val categories: List<Categories.Category>,
    val onItemClicked: (String) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        return CategoriesViewHolder(
            CategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        val category = categories[position]
        holder.displayCategories(category)
        holder.onItemClicked = onItemClicked
    }

    override fun getItemCount(): Int = categories.size

    class CategoriesViewHolder(var binding: CategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var onItemClicked: ((String) -> Unit)? = null

        fun displayCategories(category: Categories.Category) {
            val TAG = "Adapter"
            binding.tvCategoryItem.text = category.strCategory


            binding.tvCategoryItem.setOnClickListener {

                val current = category.strCategory
                //Navigate
                onItemClicked?.invoke(current)

            }
        }

    }

}
