package com.example.cocktails.view

import com.example.cocktails.model.remote.objects.ByCategory
import com.example.cocktails.model.remote.objects.ById
import com.example.cocktails.model.remote.objects.Categories

data class DrinkState(
    var isLoading: Boolean? = false,
    val errorMsg:String? = "You really screwed that up didn't you?",

    val categories: List<Categories.Category> = listOf(),
    val drinksInCategory: List<ByCategory.Drink> = listOf(),
    val oneDrink: ById.Drink = ById.Drink()
)
