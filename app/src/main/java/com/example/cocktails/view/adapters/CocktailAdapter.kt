package com.example.cocktails.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktails.databinding.IngredientsItemBinding

class CocktailAdapter(
    val ingredients: List<String>
) : RecyclerView.Adapter<CocktailAdapter.IngredientsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = IngredientsItemBinding.inflate(layoutInflater, parent, false)
        return IngredientsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        val ingredient = ingredients[position]
        holder.displayIngredients(ingredient)
    }

    override fun getItemCount(): Int = ingredients.size

    inner class IngredientsViewHolder(val binding: IngredientsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayIngredients(ingredients: String) {
            binding.tvIngredients.text = ingredients
        }
    }
}
