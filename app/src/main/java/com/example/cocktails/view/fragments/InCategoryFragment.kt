package com.example.cocktails.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktails.databinding.InCatagoryBinding
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.view.adapters.InCategoryAdapter
import com.example.cocktails.viewmodel.CocktailVMFactory
import com.example.cocktails.viewmodel.CocktailViewModel

class InCategoryFragment : Fragment() {
    private val cocktailVMFactory: CocktailVMFactory = CocktailVMFactory(CocktailRepo)
    private val viewModel by viewModels<CocktailViewModel>() { cocktailVMFactory }
    lateinit var binding: InCatagoryBinding
    lateinit var drinkState: DrinkState
    val TAG = "InCategory fragment"
    val args by navArgs<InCategoryFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InCatagoryBinding.inflate(inflater, container, false)
        viewModel.getDrinksInCategory(args.category)
        initViews()
        return binding.root
    }

    fun initViews() {
        with(binding) {
            rvInCategory.layoutManager = LinearLayoutManager(context)
            rvInCategory.adapter = InCategoryAdapter()

            viewModel.drinksState.observe(viewLifecycleOwner) { state ->

                val drinksAdapter = InCategoryAdapter(state.drinksInCategory) { strDrink ->
                    val action = InCategoryFragmentDirections
                        .actionInCategoryToCocktailFragment(strDrink)
                    findNavController().navigate(action)

                }
                rvInCategory.adapter = drinksAdapter


            }
        }
    }
}