package com.example.cocktails.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktails.databinding.InCatagoryItemBinding
import com.example.cocktails.model.remote.objects.ByCategory

class InCategoryAdapter(
    var drinks: List<ByCategory.Drink> = listOf(),
    val onDrinkClicked: (String) -> Unit = {}
) : RecyclerView.Adapter<InCategoryAdapter.DrinksViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(
           InCatagoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drink = drinks[position]
        holder.displayDrinks(drink)
        holder.onDrinkClicked = onDrinkClicked
    }

    override fun getItemCount(): Int = drinks.size

    class DrinksViewHolder(var binding: InCatagoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var onDrinkClicked: ((String) -> Unit)? = null


        fun displayDrinks(drink: ByCategory.Drink) {
            val TAG = "Drinks Adapter"

            binding.tvCategoryCocktail.text = drink.strDrink
            binding.ivCategoryCocktail.load(drink.strDrinkThumb)

            binding.ivCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                Log.d(TAG, "displayDrinks: $clicked", )
                onDrinkClicked?.invoke(clicked)

            }

            binding.tvCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                Log.d(TAG, "displayDrinks: $clicked", )
                onDrinkClicked?.invoke(clicked)

            }
        }
    }
}