package com.example.cocktails.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.example.cocktails.databinding.CocktailBinding
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.view.adapters.CocktailAdapter
import com.example.cocktails.viewmodel.CocktailVMFactory
import com.example.cocktails.viewmodel.CocktailViewModel


class CocktailFragment: Fragment() {
    private val cocktailVMFactory: CocktailVMFactory = CocktailVMFactory(CocktailRepo)
    val args by navArgs<CocktailFragmentArgs>()
    lateinit var binding: CocktailBinding
    lateinit var drinkState: DrinkState
    private val viewModel by viewModels<CocktailViewModel> { cocktailVMFactory }
    val TAG = "Cocktail fragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CocktailBinding.inflate(inflater, container, false)
        initViews()
        viewModel.getOneDrinkById(args.iD)
        return binding.root
    }

    fun initViews() {
        with(binding) {
            viewModel.drinksState.observe(viewLifecycleOwner) {state : DrinkState->
                tvCocktail.text = state.oneDrink.strDrink
                imgCocktail.load(state.oneDrink.strDrinkThumb)
                tvCocktailDescription.text = state.oneDrink.strInstructions
                Log.e(TAG, "initViews: ${state.oneDrink.toIngredientsList()}")
                val ingredients = state.oneDrink.toIngredientsList()
                rvIngredients.layoutManager = LinearLayoutManager(context)
                rvIngredients.adapter = CocktailAdapter(ingredients)

            }
        }
    }
}