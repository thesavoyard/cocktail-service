package com.example.cocktails.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktails.databinding.CategoriesBinding
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.view.DrinkState
import com.example.cocktails.view.adapters.CategoriesAdapter
import com.example.cocktails.viewmodel.CocktailVMFactory
import com.example.cocktails.viewmodel.CocktailViewModel

class CategoriesFragment : Fragment() {
    private val cocktailVMFactory: CocktailVMFactory = CocktailVMFactory(CocktailRepo)
    private val viewModel by viewModels<CocktailViewModel>() { cocktailVMFactory }
    lateinit var binding: CategoriesBinding
    lateinit var drinkState: DrinkState
    val TAG = "Categories fragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CategoriesBinding.inflate(inflater, container, false)
        viewModel.getCategories()
        initViews()
        return binding.root
    }

    fun initViews() {

        with(binding) {

            viewModel.drinksState.observe(viewLifecycleOwner) { state ->


                val categoriesAdapter = CategoriesAdapter(state.categories){ strCategory ->
                    val action = CategoriesFragmentDirections
                        .actionCategoriesToInCategory(strCategory)
                    findNavController().navigate(action)

                }
                rvCategories.adapter = categoriesAdapter
                rvCategories.layoutManager = LinearLayoutManager(context)


            }
        }
    }
}